//
//  JJMAppDelegate.h
//  SandboxBike2
//
//  Created by Jarno Mannelin on 08/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JJMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
