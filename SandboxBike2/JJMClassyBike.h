//
//  JJMClassyBike.h
//  SandboxBike2
//
//  Created by Jarno Mannelin on 17/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import <UIKit/UIKit.h>

#import "JJMScene.h"


@interface JJMClassyBike : SKNode

- (void) setup:(JJMScene*)scene inPosition:(CGPoint)position;

- (void) setThrottle:(BOOL)throttleOn;

- (void) setBreak:(BOOL)breakOn;

- (void) moveBike;

- (SKSpriteNode*) getBikeBody;

@end
