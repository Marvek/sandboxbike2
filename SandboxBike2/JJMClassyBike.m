//
//  JJMClassyBike.m
//  SandboxBike2
//
//  Created by Jarno Mannelin on 17/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import "JJMClassyBike.h"
#import "JJMScene.h"
#import "JJMDebugDataStruct.h"

@interface JJMClassyBike ()
{
   SKSpriteNode* m_bikeBody;
   
   SKShapeNode* m_rearWheel;
   SKShapeNode* m_frontWheel;
   
   SKSpriteNode* m_rearAxis;
   SKSpriteNode* m_frontAxis;
   
   SKLabelNode* m_updatesLabel;
   SKLabelNode* m_bodySpeedLabel;
   SKLabelNode* m_rearWheelSpinLabel;
   SKLabelNode* m_frontWheelSpinLabel;
   
   SKPhysicsJointSliding* m_rearSlide;
   SKPhysicsJointSliding* m_frontSlide;
   
   SKPhysicsJointPin* m_rearPin;
   SKPhysicsJointPin* m_frontPin;
   
   JJMScene* m_scene;
   
   bool m_throttleOn;
   bool m_breakOn;
   
   bool m_debug;
   JJMDebugDataStruct* m_debugData;
}
@end


@implementation JJMClassyBike


- (id)init
{
   if (self = [super init])
   {
      m_bikeBody = NULL;
      
      m_rearWheel = NULL;
      m_frontWheel = NULL;
      
      m_rearAxis = NULL;
      m_frontAxis = NULL;
      
      m_updatesLabel = NULL;
      m_bodySpeedLabel = NULL;
      m_rearWheelSpinLabel = NULL;
      m_frontWheelSpinLabel = NULL;
      
      m_rearSlide = NULL;
      m_frontSlide = NULL;
      
      m_rearPin = NULL;
      m_frontPin = NULL;
      
      m_scene = NULL;
      
      m_throttleOn = false;
      m_breakOn = false;
      
      m_debug = true;
      m_debugData = [[JJMDebugDataStruct alloc] init];
   }
   
   return self;
}



- (void) setup:(JJMScene*)scene inPosition:(CGPoint)position
{
   m_scene = scene;
   
   [self createBikeParts:position];
   [self createSuspension];
   [self setupHUD];
}



- (void) createBikeParts:(CGPoint)position
{
   NSUInteger bikeCollisionTypeMask = CollisionCategoryDynamicObject
      | CollisionCategoryGround | CollisionCategoryPlatform;
   
   // #########################################################
   // Runko
   m_bikeBody = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(120, 58)];
   m_bikeBody.position = position;
   m_bikeBody.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(m_bikeBody.size.width * 0.9f, m_bikeBody.size.height * 0.5f)];
   m_bikeBody.name = @"bikeBody";
   [m_bikeBody setTexture:[SKTexture textureWithImageNamed:@"Cycle1"]];
   m_bikeBody.zPosition = 2;
   m_bikeBody.physicsBody.mass = 10.0f;
   m_bikeBody.physicsBody.restitution = 0.0f;
   m_bikeBody.physicsBody.categoryBitMask = CollisionCategoryHull;
   m_bikeBody.physicsBody.collisionBitMask = bikeCollisionTypeMask;
   [m_scene addToForeground:m_bikeBody];
   
   // #########################################################
   // Rear wheel
   m_rearWheel = [self makeWheelShape];
   m_rearWheel.position = CGPointMake(m_bikeBody.position.x - m_bikeBody.size.width / 2 + 25, m_bikeBody.position.y - 22);
   m_rearWheel.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:20];
   m_rearWheel.physicsBody.mass = 6.0f;
   m_rearWheel.physicsBody.restitution = 0.0f;
   m_rearWheel.physicsBody.friction = 1.0f;
   m_rearWheel.name = @"rearWheel";
   m_rearWheel.physicsBody.categoryBitMask = CollisionCategoryWheel;
   m_rearWheel.physicsBody.collisionBitMask = bikeCollisionTypeMask;
   [m_scene addToForeground:m_rearWheel];
   
   SKSpriteNode *rearWheelTexture = [SKSpriteNode spriteNodeWithImageNamed:@"Wheel1"];
   rearWheelTexture.xScale = 0.116;
   rearWheelTexture.yScale = 0.116;
   [m_rearWheel addChild:rearWheelTexture];
   
   // #########################################################
   // Front wheel
   m_frontWheel = [self makeWheelShape];
   m_frontWheel.position = CGPointMake(m_bikeBody.position.x + m_bikeBody.size.width / 2 - 18, m_bikeBody.position.y - 20);
   m_frontWheel.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:20];
   m_frontWheel.physicsBody.mass = 4.0f;
   m_frontWheel.physicsBody.restitution = 0.0f;
   m_frontWheel.physicsBody.friction = 1.0f;
   m_frontWheel.name = @"frontWheel";
   m_frontWheel.strokeColor = [SKColor whiteColor];
   m_frontWheel.physicsBody.categoryBitMask = CollisionCategoryWheel;
   m_frontWheel.physicsBody.collisionBitMask = bikeCollisionTypeMask;
   [m_scene addToForeground:m_frontWheel];
   
   SKSpriteNode *frontWheelTexture = [SKSpriteNode spriteNodeWithImageNamed:@"Wheel1"];
   frontWheelTexture.xScale = 0.116;
   frontWheelTexture.yScale = 0.116;
   [m_frontWheel addChild:frontWheelTexture];
   
   // #########################################################
   // Rear axis
//   m_rearAxis = [SKSpriteNode spriteNodeWithColor:[UIColor blueColor] size:CGSizeMake(7, 7)];
//   
//   m_rearAxis.position = CGPointMake(m_bikeBody.position.x - m_bikeBody.size.width / 2 + 25, m_bikeBody.position.y - 22);
//   m_rearAxis.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:m_rearAxis.size];
//   m_rearAxis.physicsBody.mass = 0.5f;
//   m_rearAxis.physicsBody.restitution = 0.0f;
//   m_rearAxis.physicsBody.friction = 1.0f;
//   m_rearAxis.physicsBody.categoryBitMask = CollisionCategoryAxis;
//   m_rearAxis.physicsBody.collisionBitMask = bikeCollisionTypeMask;
//   [m_scene addToForeground:m_rearAxis];
   
   
   // #########################################################
   // Front axis
   m_frontAxis = [SKSpriteNode spriteNodeWithColor:[UIColor blueColor] size:CGSizeMake(7, 7)];
   
   m_frontAxis.position = CGPointMake(m_bikeBody.position.x + m_bikeBody.size.width / 2 - 18, m_bikeBody.position.y - 20);
   m_frontAxis.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:m_frontAxis.size];
   
   m_frontAxis.physicsBody.mass = 0.5f;
   m_frontAxis.physicsBody.restitution = 0.0f;
   m_frontAxis.physicsBody.friction = 1.0f;
   m_frontAxis.physicsBody.categoryBitMask = CollisionCategoryAxis;
   m_frontAxis.physicsBody.collisionBitMask = bikeCollisionTypeMask;
   [m_scene addToForeground:m_frontAxis];
   
   m_bikeBody.physicsBody.usesPreciseCollisionDetection = YES;
   m_rearWheel.physicsBody.usesPreciseCollisionDetection = YES;
   m_frontWheel.physicsBody.usesPreciseCollisionDetection = YES;
   m_rearAxis.physicsBody.usesPreciseCollisionDetection = YES;
   m_frontAxis.physicsBody.usesPreciseCollisionDetection = YES;
}



// Ref http://stackoverflow.com/questions/20257015/sprite-kit-spring-joints-shock-absorber
- (void) createSuspension
{
   // #########################################################
   // Rear suspension
   m_rearSlide = [
      SKPhysicsJointSliding jointWithBodyA:m_bikeBody.physicsBody
      bodyB:m_rearAxis.physicsBody
      anchor:CGPointMake(m_rearAxis.position.x, m_rearAxis.position.y)
      axis:CGVectorMake(0, 1)];
   
   m_rearSlide.shouldEnableLimits = TRUE;
   m_rearSlide.lowerDistanceLimit = 0;
   m_rearSlide.upperDistanceLimit = 10;
   
   //m_rearPin = [SKPhysicsJointPin jointWithBodyA:m_rearAxis.physicsBody bodyB:m_rearWheel.physicsBody anchor:m_rearWheel.position];
   m_rearPin = [SKPhysicsJointPin jointWithBodyA:m_bikeBody.physicsBody bodyB:m_rearWheel.physicsBody anchor:m_rearWheel.position];
   
   // #########################################################
   // Front suspension
   m_frontSlide = [
                  SKPhysicsJointSliding jointWithBodyA:m_bikeBody.physicsBody
                  bodyB:m_frontAxis.physicsBody
                  anchor:CGPointMake(m_frontAxis.position.x, m_frontAxis.position.y  + 1)
                  axis:CGVectorMake(0, 1)];
   
   m_frontSlide.shouldEnableLimits = TRUE;
   m_frontSlide.lowerDistanceLimit = 0;
   m_frontSlide.upperDistanceLimit = -25;
   
   m_frontPin = [SKPhysicsJointPin jointWithBodyA:m_frontAxis.physicsBody bodyB:m_frontWheel.physicsBody anchor:m_frontWheel.position];
   
   // #########################################################
   // Add to physics world
   [m_scene.physicsWorld addJoint:m_rearPin];
   [m_scene.physicsWorld addJoint:m_frontPin];
   //[m_scene.physicsWorld addJoint:m_rearSlide];
   [m_scene.physicsWorld addJoint:m_frontSlide];
   
}


- (void) setupHUD
{
   // Debug labels
   m_updatesLabel = [[SKLabelNode alloc] init];
   m_bodySpeedLabel = [[SKLabelNode alloc] init];
   m_rearWheelSpinLabel = [[SKLabelNode alloc] init];
   m_frontWheelSpinLabel = [[SKLabelNode alloc] init];
   
   m_updatesLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
   m_updatesLabel.fontSize = 14;
   m_updatesLabel.position = CGPointMake(10, 60);
   
   m_bodySpeedLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
   m_bodySpeedLabel.fontSize = 14;
   m_bodySpeedLabel.position = CGPointMake(10, 45);
   
   m_rearWheelSpinLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
   m_rearWheelSpinLabel.fontSize = 14;
   m_rearWheelSpinLabel.position = CGPointMake(10, 30);
   
   m_frontWheelSpinLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
   m_frontWheelSpinLabel.fontSize = 14;
   m_frontWheelSpinLabel.position = CGPointMake(10, 15);
   
   [m_scene addToHUD:m_updatesLabel];
   [m_scene addToHUD:m_bodySpeedLabel];
   [m_scene addToHUD:m_rearWheelSpinLabel];
   [m_scene addToHUD:m_frontWheelSpinLabel];
}


- (void) setThrottle:(BOOL)throttleOn
{
   m_throttleOn = throttleOn;
}



- (void) setBreak:(BOOL)breakOn
{
   m_breakOn = breakOn;
}



- (void) moveBike
{
   if (m_throttleOn)
   {
      [m_rearWheel.physicsBody applyTorque:(-30)];
      [m_bikeBody.physicsBody applyTorque:(1.5f)];
   }
   else if(m_breakOn)
   {
      [m_rearWheel.physicsBody applyTorque:(25)];
      [m_bikeBody.physicsBody applyTorque:(-0.5f)];
   }
   
   if(m_debug)
   {
      [self updateDebugData];
   }
}


- (void) updateDebugData
{
   m_debugData.UpdateCounter += 1;
   
   clock_t time = clock();
   
   m_debugData.DeltaUpdate = time - m_debugData.LastUpdate;
   m_debugData.LastUpdate = time;
   
   // #########################################################
   // Updates
   m_updatesLabel.text = [NSString stringWithFormat:@"Update count: %d Delta update: %f", m_debugData.UpdateCounter, (float)m_debugData.DeltaUpdate];
   
   if(0 == (int)m_debugData.UpdateCounter % (int)m_debugData.UpdateFrequency)
   {
      // #########################################################
      // Body
      CGPoint currentBikeBodyPosition = m_bikeBody.position;
      CGPoint bikeBodyDeltaPosition = CGPointMake(currentBikeBodyPosition.x - m_debugData.LastBikeBodyPosition.x, currentBikeBodyPosition.y - m_debugData.LastBikeBodyPosition.y);
      float bikeBodyDeltaMove = [m_debugData getVectorLength:(bikeBodyDeltaPosition)];
      
      if (m_debugData.LastBikeBodyPosition.x > currentBikeBodyPosition.x)
      {
         bikeBodyDeltaMove = -1 * bikeBodyDeltaMove * (60000.0f / m_debugData.DeltaUpdate);
      }
      
      m_debugData.LastBikeBodyPosition = currentBikeBodyPosition;
      
      m_bodySpeedLabel.text = [NSString stringWithFormat:@"Bike body speed: %f", bikeBodyDeltaMove];
      
      // #########################################################
      // Rear wheel
      float currentRearWheelRotation = m_rearWheel.zRotation;
      float rearWheelDeltaRotation = -(currentRearWheelRotation - m_debugData.LastRearWheelRotation) * (60000.0f / m_debugData.DeltaUpdate);
      
      m_debugData.LastRearWheelRotation = currentRearWheelRotation;
      
      m_rearWheelSpinLabel.text = [NSString stringWithFormat:@"Rear wheel rotation change: %f", rearWheelDeltaRotation];
      
      // #########################################################
      // Front wheel
      float currentFrontWheelRotation = m_frontWheel.zRotation;
      float frontWheelDeltaRotation = -(currentFrontWheelRotation - m_debugData.LastFrontWheelRotation) * (60000.0f / m_debugData.DeltaUpdate);
      m_debugData.LastFrontWheelRotation = currentFrontWheelRotation;
      
      m_frontWheelSpinLabel.text = [NSString stringWithFormat:@"Front wheel rotation change: %f", frontWheelDeltaRotation];
   }
}



- (SKSpriteNode*) getBikeBody
{
   return m_bikeBody;
}



- (SKShapeNode*) makeWheelShape
{
   SKShapeNode *wheel = [[SKShapeNode alloc] init];
   CGMutablePathRef myPath = CGPathCreateMutable();
   CGPathAddArc(myPath, NULL, 0, 0, 20, 0, M_PI*2, YES);
   wheel.path = myPath;
   wheel.physicsBody.friction = 1.0;
   
   return wheel;
}








@end

