//
//  JJMDebugDataStruct.h
//  SandboxBike2
//
//  Created by Jarno Mannelin on 23/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JJMDebugDataStruct : NSObject

@property CGPoint LastBikeBodyPosition;
@property float LastRearWheelRotation;
@property float LastFrontWheelRotation;
@property clock_t LastUpdate;
@property clock_t DeltaUpdate;

@property int UpdateCounter;
@property int UpdateFrequency;

- (float) getVectorLength:(CGPoint)vector;

@end
