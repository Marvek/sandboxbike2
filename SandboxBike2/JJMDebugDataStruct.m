//
//  JJMDebugDataStruct.m
//  SandboxBike2
//
//  Created by Jarno Mannelin on 23/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import "JJMDebugDataStruct.h"

@implementation JJMDebugDataStruct

- (id)init
{
    if (self = [super init])
    {
        _LastBikeBodyPosition = CGPointMake(0.0f, 0.0f);
        _LastRearWheelRotation = 0.0f;
        _LastFrontWheelRotation = 0.0f;
        _LastUpdate = clock();
        _DeltaUpdate = 0.0f;
        
        _UpdateCounter = 0;
        _UpdateFrequency = 5;
    }
    return self;
}


- (float) getVectorLength:(CGPoint)vector
{
    float ret = sqrt((vector.x * vector.x) + (vector.y * vector.y));
    
    return ret;
}


@end
