//
//  Foreground.h
//  Bike1a
//
//  Created by Mikko Tapaninen on 11.8.2014.
//  Copyright (c) 2014 Mikko Tapaninen. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>


@interface JJMForeground : SKSpriteNode
{
    float _lineStartX;
    float _lineStartY;
    float _lineCurrentX;
    float _lineCurrentY;
    
    NSMutableArray *_terrainLines;
}
-(void)createLineTerrain;
@end
