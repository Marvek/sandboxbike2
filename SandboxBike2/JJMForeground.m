//
//  Foreground.m
//  Bike1a
//
//  Created by Mikko Tapaninen on 11.8.2014.
//  Copyright (c) 2014 Mikko Tapaninen. All rights reserved.
//

#import "JJMForeground.h"

@implementation JJMForeground

- (id)init
{
    if ((self = [[super init] initWithImageNamed:@"Tile2"]))
	{
        // Maaston piirron aloituskohta
        _lineStartX = 100;
        _lineStartY = 300;
        
        _lineCurrentX = 0;
        _lineCurrentY = 0;
        
        _terrainLines = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)addLine: (float) lineWidth lineHeight: (float) lineHeight
{
    float lineNewX = _lineCurrentX + lineWidth;
    float lineNewY = _lineCurrentY + lineHeight;
    
    SKShapeNode *newline = [SKShapeNode node];
    CGMutablePathRef pathToDraw = CGPathCreateMutable();
    CGPathMoveToPoint(pathToDraw, NULL, _lineCurrentX, _lineCurrentY);
    CGPathAddLineToPoint(pathToDraw, NULL, lineNewX, lineNewY);
    newline.path = pathToDraw;
    newline.physicsBody = [SKPhysicsBody bodyWithEdgeFromPoint:CGPointMake(_lineCurrentX, _lineCurrentY) toPoint:CGPointMake(lineNewX, lineNewY)];
    newline.physicsBody.dynamic = NO;
    newline.physicsBody.friction = 1.0f;
    [newline setStrokeColor:[UIColor redColor]];
    
    [_terrainLines addObject: newline];
    [self addChild:newline];
    
    _lineCurrentX = lineNewX;
    _lineCurrentY = lineNewY;
}

-(void)createLineTerrain
{
    // Maasto
    _lineCurrentX = _lineStartX;
    _lineCurrentY = _lineStartY;
    
    [self addLine:50 lineHeight:400];
    [self addLine:50 lineHeight:-400];
    [self addLine:400 lineHeight:0];
    [self addLine:200 lineHeight:150];
    [self addLine:0 lineHeight:-150];
    [self addLine:300 lineHeight:0];
    [self addLine:200 lineHeight:-150];
    [self addLine:50 lineHeight:0];
    [self addLine:150 lineHeight:-20];
    [self addLine:250 lineHeight:0];
    [self addLine:50 lineHeight:5];
    [self addLine:50 lineHeight:10];
    [self addLine:50 lineHeight:20];
    [self addLine:50 lineHeight:30];
    [self addLine:50 lineHeight:40];
    [self addLine:50 lineHeight:50];
    [self addLine:50 lineHeight:60];
    [self addLine:50 lineHeight:70];
    [self addLine:50 lineHeight:-10];
    [self addLine:50 lineHeight:-20];
    [self addLine:50 lineHeight:-30];
    [self addLine:50 lineHeight:-40];
    [self addLine:50 lineHeight:-50];
    [self addLine:50 lineHeight:-60];
    [self addLine:100 lineHeight:-60];
    [self addLine:100 lineHeight:-50];
    [self addLine:200 lineHeight:-40];
    [self addLine:300 lineHeight:-30];
    [self addLine:400 lineHeight:0];
}

@end
