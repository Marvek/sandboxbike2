//
//  JJMMyScene.h
//  SandboxBike2
//

//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>


typedef NS_OPTIONS(NSUInteger, CollisionCategory)
{
    CollisionCategoryHull          = 0x1 << 0,
    CollisionCategoryAxis          = 0x1 << 1,
    CollisionCategoryWheel         = 0x1 << 2,
    CollisionCategoryPlatform      = 0x1 << 3,
    CollisionCategoryGround        = 0x1 << 4,
    CollisionCategoryDynamicObject = 0x1 << 5
};


@interface JJMScene : SKScene

- (void) addToForeground:(SKNode*)node;

- (void) addToHUD:(SKNode*)node;

@end
