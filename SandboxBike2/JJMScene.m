//
//  JJMMyScene.m
//  SandboxBike2
//
//  Created by Jarno Mannelin on 08/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import "JJMScene.h"
#import "JJMClassyBike.h"
#import "JJMForeground.h"



@interface JJMScene ()
{
    SKNode* m_centerObject;
    
    SKNode* m_backgroundNode;
    JJMForeground* m_foregroundNode;
    SKNode* m_hudNode;
    
    SKShapeNode *m_rearWheel;
    SKShapeNode *m_frontWheel;
    
    JJMClassyBike* m_classyBike;
    
    bool m_throttleOn;
    bool m_breakOn;
    
    bool m_paused;
    
    int m_bikeStartPositionX;
    int m_bikeStartPositionY;
}
@end


@implementation JJMScene


-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
    {
        m_centerObject = NULL;
        
        m_backgroundNode = NULL;
        m_foregroundNode = NULL;
        m_hudNode = NULL;
        
        m_classyBike = NULL;
        
        m_paused = false;
        
        m_bikeStartPositionX = 400;
        m_bikeStartPositionY = 500;

        NSLog(@"initWithSize - Size: %@", NSStringFromCGSize(size));
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        self.physicsWorld.gravity = CGVectorMake(0, -6.0f);
        
        [self createBackgroundNode];
        [self createForegroundNode];
        [self createHUDNode];
        [self createClassyBike:CGPointMake(
            m_bikeStartPositionX, m_bikeStartPositionY)];
        
        SKAction *wait = [SKAction waitForDuration:0.02];
        SKAction *performSelector = [SKAction performSelector:@selector(moveBike) onTarget:m_classyBike];
        SKAction *sequence = [SKAction sequence:@[performSelector, wait]];
        SKAction *moveBikeTimer = [SKAction repeatActionForever:sequence];
        [self runAction:moveBikeTimer];
        
        m_foregroundNode.position = CGPointMake(0, -200);
    }
    
    return self;
}



-(void)update:(CFTimeInterval)currentTime
{
    if(m_paused)
    {
        
    }
    else
    {
        m_backgroundNode.position = CGPointMake(
            m_bikeStartPositionX - (m_centerObject.position.x / 10),
            -200 + m_bikeStartPositionY + (-m_centerObject.position.y / 10));
        // Height, width reversed as the screen is horizontal
        m_foregroundNode.position = CGPointMake(
            -m_centerObject.position.x + [UIScreen mainScreen].bounds.size.height / 2,
            -m_centerObject.position.y + [UIScreen mainScreen].bounds.size.width / 2);
    }
}



-(SKSpriteNode*)newPlatform:(CGSize)size
{
    SKSpriteNode* ret = [[SKSpriteNode alloc]initWithColor:
                         [SKColor greenColor] size:size];
    ret.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:ret.size];
    ret.physicsBody.dynamic = NO;
    
    return ret;
}



-(void) createClassyBike:(CGPoint)position
{
    m_classyBike = [[JJMClassyBike alloc] init];

    [m_classyBike setup:self inPosition:position];
    
    m_centerObject = [m_classyBike getBikeBody];
}



- (void) addToForeground:(SKNode*)node
{
    [m_foregroundNode addChild:node];
}



- (void) addToHUD:(SKNode *)node
{
    [m_hudNode addChild:node];
}



- (SKShapeNode*) makeWheelShape
{
    SKShapeNode *wheel = [[SKShapeNode alloc] init];
    CGMutablePathRef myPath = CGPathCreateMutable();
    CGPathAddArc(myPath, NULL, 0, 0, 20, 0, M_PI*2, YES);
    wheel.path = myPath;
    wheel.physicsBody.friction = 1.0;
    
    return wheel;
}



- (void) createBackgroundNode
{
    m_backgroundNode = [SKSpriteNode spriteNodeWithImageNamed:@"Background1"];
    m_backgroundNode.xScale = 2;
    m_backgroundNode.yScale = 2;
    [self addChild:m_backgroundNode];
}



- (void) createForegroundNode
{
    // Create the node
    m_foregroundNode = [[JJMForeground alloc] init];
    [self addChild:m_foregroundNode];
    
    // Lisätään maasto
    [m_foregroundNode createLineTerrain];
}


- (void) createHUDNode
{
    m_hudNode = [[SKNode alloc] init];
    [self addChild:m_hudNode];
}


-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    /* Called when a touch begins */
    for (UITouch* touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if (location.x > self.size.width/2)
        {
            [m_classyBike setThrottle:(TRUE)];
        }
        else
        {
            [m_classyBike setBreak:(TRUE)];
        }
    }
}



-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [m_classyBike setThrottle:(false)];
    [m_classyBike setBreak:(false)];
}

@end

