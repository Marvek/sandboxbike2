//
//  main.m
//  SandboxBike2
//
//  Created by Jarno Mannelin on 08/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JJMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JJMAppDelegate class]));
    }
}
