//
//  SandboxBike2Tests.m
//  SandboxBike2Tests
//
//  Created by Jarno Mannelin on 08/08/14.
//  Copyright (c) 2014 Jarno Mannelin. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SandboxBike2Tests : XCTestCase

@end

@implementation SandboxBike2Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
